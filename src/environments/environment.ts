// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD_xlo3ZnGHMEJa-YsMDavA8ssKrg240ho',
    authDomain: 'hohen-f0be1.firebaseapp.com',
    databaseURL: 'https://hohen-f0be1.firebaseio.com',
    projectId: 'hohen-f0be1',
    storageBucket: 'hohen-f0be1.appspot.com',
    messagingSenderId: '685774569039'
  }
};
