import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './form.component';
import {ReactiveFormsModule} from '@angular/forms';
import { SuggestionPreviewComponent } from './suggestion-preview/suggestion-preview.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [FormComponent, SuggestionPreviewComponent],
  exports: [FormComponent, SuggestionPreviewComponent]
})
export class FormModule { }
