import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import {Suggestion} from '../interfaces/suggestion';
import {SearchService} from '../card-list/search.service';
import {IGenre} from '../interfaces/i-genre';
import {of} from 'rxjs/observable/of';
import {IMan} from '../interfaces/i-man';

@Component({
  selector: 'hw-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Output() passURL = new EventEmitter<string>();
  private KEY = 'af5068344a951bdf9c0a425ca175b44e';

  genres: IGenre[] = [];
  formedTitles: Suggestion[] = [];
  formedPeople: Suggestion[] = [];

  form: FormGroup;

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(''),
      genre: new FormControl(''),
      actor: new FormControl(''),
      crew: new FormControl(''),
      getID: new FormControl('')
    });

    this.searchService.getGenres().subscribe(
      data => this.genres = data
    );

    this.form.get('title').valueChanges.debounceTime(400).distinctUntilChanged().
      switchMap(title => title ? this.searchService.getTitle(title) : of([]) ).
      subscribe(result => this.formedTitles = this.unique(result));

      this.form.get('getID').valueChanges.debounceTime(400).distinctUntilChanged().
      switchMap(man => man ? this.searchService.getPeople(man) : of([]) ).
      subscribe(result => this.formedPeople = this.formPeople(result));
  }

  unique(arr: string[]): Suggestion[] {
    const tempArr = Array.from(new Set(arr));

    return tempArr.map(str => ({ name: str, id: '' })).slice(0, Math.min(tempArr.length, 5));
  }

  formPeople(men: IMan[]): Suggestion[] {
    return men.map(man => ({ name: man.name, id: man.id})).slice(0, Math.min(men.length, 10));
  }

  onTitleSug(suggestion: string) {
    this.form.get('title').setValue(suggestion);
    this.formedTitles = [];
  }

  onSubmitFeatures() {
    let url = 'https://api.themoviedb.org/3/discover/movie?api_key=' + `${this.KEY}` +
      '&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false';

    if (this.form.get('genre').value) {
      url += '&with_genres=' + this.form.get('genre').value;
    }

    if (this.form.get('actor').value) {
      url += '&with_cast=' + this.form.get('actor').value;
    }

    if (this.form.get('crew').value) {
      url += '&with_crew=' + this.form.get('crew').value;
    }

    this.passURL.emit(url);
    this.form.reset();
  }

  onSubmitTitle() {
    this.passURL.emit('https://api.themoviedb.org/3/search/movie?api_key=' + `${this.KEY}` +
      '&language=en-US&include_adult=false&query=' + this.form.get('title').value);
    this.form.reset();
  }

  noFeatures(): boolean {
    return !(this.form.get('genre').value || this.form.get('actor').value || this.form.get('crew').value);
  }
}
