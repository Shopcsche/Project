import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Suggestion} from '../../interfaces/suggestion';

@Component({
  selector: 'hw-suggestion-preview',
  templateUrl: './suggestion-preview.component.html',
  styleUrls: ['./suggestion-preview.component.css']
})
export class SuggestionPreviewComponent {
  @Input() suggestion: Suggestion;
  @Output() onClick =  new EventEmitter<string>();

  onChoice() {
    this.onClick.emit(this.suggestion.name);
  }
}
