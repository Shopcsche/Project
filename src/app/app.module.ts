import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import {registerLocaleData} from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { AppComponent } from './app.component';

import { CardListModule } from './card-list/card-list.module';
import { RegFormModule } from './reg-form/reg-form.module';

import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import {SideBarModule} from './side-bar/side-bar.module';
import {MovieService} from './movie.service';
import {RouterModule} from '@angular/router';
import {appRoutes} from './app.routing';
import {NotFoundModule} from './not-found/not-found.module';
import {MovieListService} from './movie-list/movie-list.service';
import {AuthService} from './auth.service';
import {SearchService} from './card-list/search.service';
import {ListContainerModule} from './list-container/list-container.module';
import {MovieListModule} from './movie-list/movie-list.module';

registerLocaleData(localeRu);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CardListModule,
    RegFormModule,
    MovieListModule,
    SideBarModule,
    NotFoundModule,
    ListContainerModule,
    AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [{provide: LOCALE_ID, useValue: 'ru'}, MovieService, MovieListService, AuthService, SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
