import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IUser} from '../interfaces/i-user';
import {MovList} from '../interfaces/mov-list';
import {AuthService} from '../auth.service';
import {MovieListService} from '../movie-list/movie-list.service';
import {Router} from '@angular/router';

@Component({
  selector: 'hw-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  user: Observable<IUser>;
  lists: Observable<MovList[]>;

  constructor(private movieListService: MovieListService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.lists = this.movieListService.getLists();
    this.user = this.authService.getUser$();
  }

  logout() {
    this.authService.logout();
  }

  onEnter() {
    this.router.navigate(['/login']);
  }

  isAuthorized(): string {
    return this.authService.isAuthorized();
  }
}
