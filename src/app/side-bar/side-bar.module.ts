import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SideBarComponent} from './side-bar.component';
import {ListContainerModule} from '../list-container/list-container.module';

@NgModule({
  imports: [
    CommonModule,
    ListContainerModule
  ],
  declarations: [SideBarComponent],
  exports: [SideBarComponent]
})
export class SideBarModule { }
