import {Routes} from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';
import {CardListComponent} from './card-list/card-list.component';
import {RegFormComponent} from './reg-form/reg-form.component';

export const appRoutes: Routes = [
  {
    path: 'login',
    component: RegFormComponent
  },
  {
    path: '',
    component: CardListComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
