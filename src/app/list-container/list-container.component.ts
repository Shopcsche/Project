import { Component, OnInit } from '@angular/core';
import {MovList} from '../interfaces/mov-list';
import {Observable} from 'rxjs/Observable';
import {MovieListService} from '../movie-list/movie-list.service';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {MovieService} from '../movie.service';
import {AngularFireDatabase} from 'angularfire2/database';

@Component({
  selector: 'hw-list-container',
  templateUrl: './list-container.component.html'
})
export class ListContainerComponent implements OnInit {
  asyncLists: Observable<MovList[]>;
  currentOpen: string = null;

  constructor(private db: AngularFireDatabase,
              private movieListService: MovieListService,
              private movieService: MovieService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.asyncLists = this.movieListService.getLists();
  }

  addList() {
    if (!this.authService.isAuthorized()) {
      this.router.navigate(['/login']);

      return;
    }

    this.movieListService.addList().subscribe();
  }

  isCurrentOpen(id: string): boolean {
    return id === this.currentOpen;
  }

  onListClick(id: string) {
    if (id === this.currentOpen) {
      this.currentOpen = null;

      return;
    }

    this.currentOpen = id;
    this.movieService.changeCurrent(id);
  }

}
