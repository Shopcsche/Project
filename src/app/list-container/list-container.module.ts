import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListContainerComponent} from './list-container.component';
import {MovieListModule} from '../movie-list/movie-list.module';

@NgModule({
  imports: [
    CommonModule,
    MovieListModule
  ],
  declarations: [ListContainerComponent],
  exports: [ListContainerComponent]
})
export class ListContainerModule { }
