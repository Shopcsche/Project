import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {RegFormComponent} from './reg-form.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [RegFormComponent],
  exports: [RegFormComponent]
})
export class RegFormModule { }
