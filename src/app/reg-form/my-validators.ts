import { FormControl, FormGroup, Validators } from '@angular/forms';

export class MyValidators extends Validators {

  static symbols(control: FormControl): { [key: string]: any } {
    return /\d/.test(control.value) && /[a-z]/.test(control.value) && /[A-Z]/.test(control.value) &&
           /[_!#+$-]/.test(control.value) ? null : {invalid : true};
  }

  static isDifferent(group: FormGroup): { [key: string]: any } {
    return group.get('pwd').value === group.get('pwdRep').value ? null : {invalid: true};
  }
}
