import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import { MyValidators } from './my-validators';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs/Observable';
import {ILogin} from '../interfaces/i-login';

const AUTH_ERROR_CODE = 'auth/wrong-password';
const EMAIL_ERROR_CODE = 'auth/invalid-email';
const errorMessages = {
  DEFAULT: 'Неизвестная ошибка. Попробуйте позже.',
  AUTH_ERROR_CODE: 'Неверные имя пользователя и пароль.'
};

@Component({
  selector: 'hw-reg-form',
  templateUrl: './reg-form.component.html',
  styleUrls: ['./reg-form.component.css']
})

export class RegFormComponent implements OnInit {
  form: FormGroup;
  submitAttempt = false;
  errorMessage = '';

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.maxLength(100), Validators.email]),
      pwds: new FormGroup ( {
        pwd: new FormControl('', [Validators.required, Validators.minLength(5), MyValidators.symbols]),
        pwdRep: new FormControl('')}, [MyValidators.isDifferent]),
    });
  }

  getEmail(): AbstractControl {
    return this.form.get('email');
  }

  getRep(): AbstractControl {
    return this.form.get('pwds').get('pwdRep');
  }

  getPwd(): AbstractControl {
    return this.form.get('pwds').get('pwd');
  }

  willShow(control: AbstractControl): boolean {
    return control.touched || this.submitAttempt;
  }

  onSignUp() {
    this.submitAttempt = true;

    if (this.form.invalid) {
      return;
    }

    this.submitAttempt = false;
    this.handleAuth(this.authService.signUp.bind(this.authService));
  }

  onLogin() {
    this.handleAuth(this.authService.login.bind(this.authService));
  }

  private handleAuth(authMethod: (data: ILogin) => Observable<any>) {
    const email = this.getEmail().value;
    const password = this.getPwd().value;

    this.errorMessage = '';
    authMethod({email, password})
      .subscribe(() => {
          this.form.reset();
        },
        (error) => {
          this.handleLoginError(error);
        });
  }

  private handleLoginError(error: any) {
    console.log('--- error', error);
    let errorMessage = errorMessages.DEFAULT;

    if (error && error.code === AUTH_ERROR_CODE) {
      errorMessage = errorMessages.AUTH_ERROR_CODE;
    }

    if (error && error.code === EMAIL_ERROR_CODE) {
      errorMessage = 'Email';
    }

    this.errorMessage = errorMessage;
  }

}
