import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../auth.service';
import {MovList} from '../interfaces/mov-list';

@Injectable()
export class MovieListService {
  constructor(private db: AngularFireDatabase,
              private authService: AuthService) {
  }

  getLists(): Observable<MovList[]> {
    return this.authService
      .getUserId$()
      .switchMap((uid) => {
        if (!uid) {
          return Observable.of([]);
        }

        return this.db.list(`users/${uid}/lists`)
          .snapshotChanges()
          .map((snapshots) => {
            return snapshots.map(({key, payload}) => {
              return {...payload.val(), id: key};
            });
          });
      });
  }

  addList(): Observable<boolean> {
    return this.authService
      .getUserId$()
      .filter(uid => !!uid)
      .map(uid => this.db.list(`users/${uid}/lists`))
      .switchMap(list => list.push({
        id: '',
        name: 'Новый Лист',
      }));
  }

  updateList(movlist: MovList): Observable<void> {
    return this.authService
      .getUserId$()
      .filter(uid => !!uid)
      .map((uid) => this.db.object(`users/${uid}/lists/${movlist.id}`))
      .switchMap(object => object.update(movlist));
  }

}
