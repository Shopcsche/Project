import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IMovie} from '../interfaces/i-movie';
import {Observable} from 'rxjs/Observable';
import {MovList} from '../interfaces/mov-list';
import {MovieListService} from './movie-list.service';
import {MovieService} from '../movie.service';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'hw-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  @Input() movies: MovList;
  @Input() isOpen = false;
  @Output() listClick = new EventEmitter<void>();
  @Output() listDelete = new EventEmitter<void>();

  asyncMovies: Observable<IMovie[]>;
  asyncFilm: Observable<string>;

  private updateNameSubj = new Subject<string>();

  constructor(private movieService: MovieService,
              private movieListService: MovieListService) { }

  ngOnInit() {
    this.setUpNameChanges();

    if (this.movies) {
      this.setMovies();

      return;
    }
  }

  changeTitle(title) {
    this.updateNameSubj.next(title);
  }

  onDeleteMovie({id}: IMovie) {
    this.movieService.deleteMovie(this.movies.id, id).subscribe();
  }

  toggleOpen() {
    if (this.movies) {
      this.setMovies();
    }

    this.listClick.emit();
  }

  toggleDelete() {
     this.movieService.deleteList(this.movies.id).subscribe();
  }

  getRandom() {
    this.asyncFilm = this.movieService.getRandom(this.movies.id);
  }

  private setMovies() {
    this.asyncMovies = this.movieService.setMovies(this.movies.id);
  }

  private setUpNameChanges() {
    this.updateNameSubj
      .map(name => name.trim())
      .map(name => name.length <= 20 ? name : name.substr(0, 20))
      .filter(name => !!name)
      .debounceTime(2000)
      .distinctUntilChanged()
      .switchMap((name) => this.movieListService.updateList({...this.movies, name}))
      .subscribe();
  }
}
