import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IMovie} from '../../interfaces/i-movie';

@Component({
  selector: 'hw-movie-preview',
  templateUrl: './movie-preview.component.html',
  styleUrls: ['./movie-preview.component.css']
})
export class MoviePreviewComponent {
  @Input() movie: IMovie;
  @Output() movieDelete = new EventEmitter<IMovie>();

  toggleDelete(event: MouseEvent) {
    event.stopPropagation();
    this.movieDelete.emit(this.movie);
  }

}
