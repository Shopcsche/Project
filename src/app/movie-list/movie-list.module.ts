import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieListComponent } from './movie-list.component';
import { MoviePreviewComponent } from './movie-preview/movie-preview.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MovieListComponent, MoviePreviewComponent],
  exports: [MovieListComponent, MoviePreviewComponent]
})
export class MovieListModule { }
