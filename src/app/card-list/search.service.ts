import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IResult} from '../interfaces/i-result';
import {Observable} from 'rxjs/Observable';
import {IGenre} from '../interfaces/i-genre';
import {IMan} from '../interfaces/i-man';

@Injectable()
export class SearchService {
  private KEY = 'af5068344a951bdf9c0a425ca175b44e';
  private URL = 'https://api.themoviedb.org/3/';

  constructor(private http: HttpClient) {
  }

  getMovies(url: string): Observable<IResult> {
    return this.http.get<IResult>(url);
  }

  getGenres(): Observable<IGenre[]> {
    return this.http.get<{genres: IGenre[]}>(`${this.URL}genre/movie/list?api_key=${this.KEY}`).map(data => data.genres);
  }

  getPeople(man: string): Observable<IMan[]> {
    return this.http.get<{results: IMan[]}>(`${this.URL}search/person?api_key=${this.KEY}&query=${man}`).map(result => result.results);
  }

  getTitle(title: string): Observable<string[]> {
    return this.http.get<{results: {title: string}[]}>(`${this.URL}search/movie?api_key=${this.KEY}&query=${title}`).map(result => result.results.map(suggestion => suggestion.title));
  }

}
