import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardListComponent } from './card-list.component';
import { FormModule } from '../form/form.module';
import { CardPreviewComponent } from './card-preview/card-preview.component';

@NgModule({
  imports: [
    CommonModule,
    FormModule
  ],
  declarations: [CardListComponent, CardPreviewComponent],
  exports: [CardListComponent, CardPreviewComponent]
})
export class CardListModule { }
