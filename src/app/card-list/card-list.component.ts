import {Component, OnInit} from '@angular/core';
import {IMovie} from '../interfaces/i-movie';
import {SearchService} from './search.service';
import {IResult} from '../interfaces/i-result';
import {IGenre} from '../interfaces/i-genre';

@Component({
  selector: 'hw-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit {
  private URL = '';
  cards: IMovie[] = [];
  genres: IGenre[] = [];
  page = 0;
  pageMax: number;

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.getGenres().subscribe(
      data => this.genres = data
    );
  }

  getGenre(id: number): string {
    for (const genre of this.genres) {
      if (genre.id === id) {
        return genre.name;
      }
    }

    return 'UNKNOWNGENRE';
  }

  transformGenres(genre_ids: number[]): string {
    if (!this.genres.length) {
      return 'СЕРВЕР УМЕР :С';
    }

    const genreArr: string[] = [];

    for (const id of genre_ids) {
      genreArr.push(this.getGenre(id));
    }

    if (genre_ids.length) {
      return genreArr.slice(0, Math.min(genre_ids.length, 3)).join(', ');
    }

      return 'NOGENRES :(';
  }

  private structMovies(result: IResult) {
    this.page = result.page;
    this.pageMax = result.total_pages;
    this.cards = [];

    for (const movie of result.results) {
      const movieObj: IMovie = {
        id: null,
        title: movie.title,
        releaseDate: movie.release_date,
        genre: this.transformGenres(movie.genre_ids),
        voteAv: movie.vote_average,
        description: movie.overview
      };

      this.cards.push(movieObj);
    }
  }

  getMovies(url: string) {
    this.URL = url;
    this.searchService.getMovies(url).subscribe(
      data => this.structMovies(data)
    );
  }

  changePage(page: number) {
    this.getMovies(`${this.URL}&page=${page}`);
  }
}
