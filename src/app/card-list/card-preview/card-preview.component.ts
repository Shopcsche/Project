import {Component, Input} from '@angular/core';
import {IMovie} from '../../interfaces/i-movie';
import {MovieService} from '../../movie.service';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';


@Component({
  selector: 'hw-card-preview',
  templateUrl: './card-preview.component.html',
  styleUrls: ['./card-preview.component.css']
})
export class CardPreviewComponent {
  @Input() movie: IMovie;

  constructor(private service: MovieService,
              private router: Router,
              private movieService: MovieService,
              private authService: AuthService) { }


  onAdd() {
    if (!this.authService.isAuthorized()) {
      this.router.navigate(['/login']);
      return;
    }

    this.movieService.addMovie(this.movie).subscribe();
  }

}
