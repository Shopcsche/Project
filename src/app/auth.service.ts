import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {AngularFireAuth} from 'angularfire2/auth';
import {Router} from '@angular/router';
import {IUser} from './interfaces/i-user';
import {ILogin} from './interfaces/i-login';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthService {
  private user$ = new BehaviorSubject<IUser>(null);

  redirectUrl: string;

  constructor(private firebaseAuth: AngularFireAuth,
              private router: Router) {
    this.firebaseAuth.authState
      .subscribe((authState) => {
        if (!authState) {
          this.user$.next(null);
          return;
        }

        this.user$.next({
          email: authState.email,
          uid: authState.uid
        });
      });
  }

  getUser$(): Observable<IUser> {
    return this.user$.asObservable();
  }

  getUserId$(): Observable<string> {
    return this.getUser$()
      .map((user) => user ? user.uid : null);
  }

  login({email, password}: ILogin): Observable<void> {
    return Observable.fromPromise(
      this.firebaseAuth
        .auth
        .signInWithEmailAndPassword(email, password)
    )
      .do(() => {
        const url = this.redirectUrl || '/';

        this.router.navigate([url]);
      });
  }

  signUp({email, password}: ILogin): Observable<void> {
    return Observable.fromPromise(
      this.firebaseAuth
        .auth
        .createUserWithEmailAndPassword(email, password)
    ).do(() => {
      const url = this.redirectUrl || '/';

      this.router.navigate([url]);
    });
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut()
      .then(() => {
        if (this.router.url.startsWith('/lists')) {
          this.router.navigate(['/']);
        }
      });
  }

  isAuthorized(): string {
    return this.user$.getValue() ? this.user$.getValue().uid : null;
  }
}
