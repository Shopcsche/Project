export interface IResult {
  page: number;
  total_pages: number;
  results: {title: string; release_date: string; genre_ids: number[], vote_average: number, overview: string}[];
}
