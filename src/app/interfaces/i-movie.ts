export interface IMovie {
  id: string;
  title: string;
  releaseDate: string;
  genre: string;
  voteAv: number;
  description: string;
}
