export interface Suggestion {
  name: string;
  id: string;
}
