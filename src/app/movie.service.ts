import { Injectable } from '@angular/core';
import {IMovie} from './interfaces/i-movie';
import {Observable} from 'rxjs/Observable';
import {AngularFireDatabase} from 'angularfire2/database';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/share';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {AuthService} from './auth.service';

@Injectable()
export class MovieService {
  private currentOpen = new BehaviorSubject<string>(null);

  constructor(private db: AngularFireDatabase,
              private authService: AuthService) { }

  addMovie(movie: IMovie): Observable<void> {
    if (!this.currentOpen.getValue()) {
      return Observable.of(null);
    }

    return Observable.of(this.db.list('movies').push(movie))
      .switchMap(({key}) => {
        return this.db.object(`moviesOfLists/${this.currentOpen.getValue()}`)
          .update({[key]: true});
      });
  }

  deleteMovie(listID: string, movieID: string): Observable<void> {
    return Observable.of(this.db.object(`moviesOfLists/${listID}/${movieID}`).remove())
      .switchMap(() => this.db.object(`movies/${movieID}`).remove());
  }

  setMovies(listID: string): Observable<IMovie[]> {
    return this.db.list(`moviesOfLists/${listID}`)
      .snapshotChanges()
      .map(snaps => snaps.map(({key}) => key))
      .switchMap((keys) => {
        if (!keys.length) {
          return Observable.of([]);
        }

        const observables = keys.map(key => {
          return this.db.object<IMovie>(`movies/${key}`)
            .valueChanges()
            .map(movie => ({...movie, id: key}));
        });

        return Observable.combineLatest(observables);
      })
      .share();
  }

  deleteList(id: string): Observable<void> {
    const uid = this.authService.isAuthorized();
    return this.db.list(`moviesOfLists/${id}`)
      .snapshotChanges()
      .map(snaps => snaps.map(({key}) => key))
      .switchMap((keys) => {
        if (!keys.length) {
          const obs = [];
          obs.push(Observable.of(this.db.object(`users/${uid}/lists/${id}`).remove()));
          return obs;
        }

        const observables = keys.map(key => {
          return Observable.of(this.db.object(`movies/${key}`).remove());
        });
        observables.push(Observable.of(this.db.object(`users/${uid}/lists/${id}`).remove()));
        observables.push(Observable.of(this.db.object(`moviesOfLists/${id}`).remove()));
        return Observable.combineLatest(observables);
      }).share();
  }

  getRandom(id: string): Observable<string> {
    return this.db.list(`moviesOfLists/${id}`)
      .snapshotChanges()
      .map(snaps => snaps.map(({key}) => key))
      .switchMap((keys) => {
        return this.db.object(`movies/${keys[Math.floor(Math.random() * keys.length)]}`).valueChanges();
      }).map((data: {title: string}) => data.title);
  }

  changeCurrent(id: string) {
    this.currentOpen.next(id);
  }
}
